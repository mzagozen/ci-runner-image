# Default to buster. These are duplicated in the .gitlab-ci.yml file, but we
# need them here too for local builds.
CURRENT_RELEASE?=buster
RELEASE?=buster
CI_REGISTRY_IMAGE?=ci-runner-image

ifneq ($(CI_PIPELINE_ID),)
DOCKER_TAG=$(CI_PIPELINE_ID)-$(RELEASE)
else
DOCKER_TAG=$(shell whoami)-$(RELEASE)
endif

build:
	docker build --build-arg RELEASE=$(RELEASE) -t $(CI_REGISTRY_IMAGE):$(DOCKER_TAG) .

push:
	docker push $(CI_REGISTRY_IMAGE):$(DOCKER_TAG)

tag-release:
	docker tag $(CI_REGISTRY_IMAGE):$(DOCKER_TAG) $(CI_REGISTRY_IMAGE):$(RELEASE)
ifeq ($(RELEASE),$(CURRENT_RELEASE))
	docker tag $(CI_REGISTRY_IMAGE):$(DOCKER_TAG) $(CI_REGISTRY_IMAGE):latest
endif

push-release:
	docker push $(CI_REGISTRY_IMAGE):$(RELEASE)
ifeq ($(RELEASE),$(CURRENT_RELEASE))
	docker push $(CI_REGISTRY_IMAGE):latest
endif
